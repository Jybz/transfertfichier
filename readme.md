Transfert de fichier :
Voici deux paires de scripts pour transférer un fichier entre deux ordinateurs de manière chiffré.
Deux scripts (un client un serveur) pour gérer la réception sur un ordinateur avec un port ouvert, deux autres (un client un serveur) pour gérer l'emission sur un ordinateur avec un port ouvert.
La gestion réseau est confié à ncat (similaire à netcat et nc) et le chiffrement est confié à openssl.
